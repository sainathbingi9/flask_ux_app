FROM python:3.8-slim-buster

WORKDIR /ux_app

COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

COPY . .
ENTRYPOINT [ "python3" ]
CMD [ "ux_app.py" ]