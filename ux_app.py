from flask import Flask, render_template, request
import requests

app = Flask(__name__)


@app.route('/')
def home():
    return render_template("index.html")


@app.route('/users', methods=['POST'])
def front_end():
    respFromCheckbox = [0,0,0,0]
    if request.form.get('All'):
        respFromCheckbox.insert(3, 1)
    else:
        if request.form.get('Name'):
            respFromCheckbox.insert(0, 1)
        if request.form.get('Id'):
            respFromCheckbox.insert(1, 1)
        if request.form.get('Token'):
            respFromCheckbox.insert(2, 1)
    base_url = "http://35.238.24.218:5000/"
    data = requests.get(base_url+"Details/{}/{}/{}/{}".format(respFromCheckbox[0],respFromCheckbox[1],respFromCheckbox[2],respFromCheckbox[3]))
    users = data.json()
    keys = users[0].keys()
    return render_template('users.html',users=users,keys=keys)


if __name__ == "__main__":
    app.run(host='0.0.0.0',port=5001)
